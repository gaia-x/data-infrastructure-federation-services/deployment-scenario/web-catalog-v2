import Keycloak from "keycloak-js";

const keycloak = new Keycloak({
  url: process.env.REACT_APP_KEYCLOAK_URL,
  realm: "FederatedCatalog",
  clientId: "FederatedCatalog"
});

export default keycloak;