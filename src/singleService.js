
import './media-query.css'
import './singleService.css'
import DataSet from './dataSet';
import cube from './assets/cube.png';
import api from '../src/services/api';
import aster from './assets/logo.png';
import globe from './assets/globe.png';
import check from './assets/check.png';
import Tooltip from '@mui/material/Tooltip';
import settings from './assets/settings.png';
import loadingGif from './assets/loading.gif';
import aster_ces from './assets/logo-ces.png';
import { Button, Modal } from "react-bootstrap";
import React, {useState, useEffect} from 'react';
import { useKeycloak } from "@react-keycloak/web";
import Offcanvas from 'react-bootstrap/Offcanvas';
import providerIcon from './assets/provider-icon.png';
import VerifyVcModal from './components/verifyVcModal';
import CircularProgress from "@mui/material/CircularProgress";
import negotiationTool from "./services/negociationTool/negociationTool";
import { Backdrop, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";

export default function SingleServices(props) {

    const { keycloak } = useKeycloak();
    const [show, setShow] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [loading, setLoading] = useState(false);
    const [loadingS, setLoadingS] = useState(false);
    const [modal, setModal] = useState(false);
    const [valuedialog, setValueDialog] = useState('');
    const [showMessage, setshowMessage] = useState(false);
    const [showMore, setShowMore] = useState(false);

    const [verifyVcModal, setVerifyVcModal] = useState(false);
    const [responseVerifierVC, setResponseVerifierVC] = useState('');
    const [loadVerifVc, setLoadVerifVc] = useState(false);


    const handleCloseDialog = () => {
        setVerifyVcModal(false);
        setLoadVerifVc(false)
    };

    const transformService = (data) => {
        let formattedService = {};
        let certif=[];

        formattedService['gx-service-offering:complianceVersion'] = '22.10';
        formattedService['compliance_label_level']="";
        data.service.forEach((vc) => {
            const subject = vc.credentialSubject;

            if(!subject && (vc['type'] || vc['@type']) && (vc['type'] || vc['@type']) === "aster-conformity:ComplianceReference" && vc["aster-conformity:hasComplianceReferenceTitle"]){
               certif.push(vc["aster-conformity:hasComplianceReferenceTitle"])
            }
            if(Array.isArray(subject)){
                const vcCompliance=subject.find(objet => objet['gx:type'] === "gx:ServiceOffering" );
                if (typeof vcCompliance === 'object' && vcCompliance !== null) {
                    const tabtype = vc.issuer.split(":");
                    const type_issuer= tabtype[tabtype.length - 1];
                    formattedService['gx-service-offering:complianceVersion']= vcCompliance['gx:version'] ? vcCompliance['gx:version'] : undefined;
                    formattedService["gx:complianceType"] = type_issuer.charAt(0).toUpperCase() + type_issuer.slice(1);
                }
            }
            const setTypeProperties = (type) => {
                formattedService['id'] = subject[type === 'gx:ServiceOffering' ? 'id' : '@id'];
                formattedService['gx-service-offering:title'] = subject['gx:name'] ? subject['gx:name'] : subject['gx:ServiceName'];
                formattedService['gx-service-offering:description'] = subject['gx:description'] ? subject['gx:description'] : undefined;
                subject['gx:keyword'] ? formattedService['gx-service-offering:serviceType']=subject['gx:keyword'] : formattedService['gx-service-offering:serviceType']=[];
                formattedService['gx-service-offering:layer'] = (subject['aster-conformity:layer'] && typeof subject['aster-conformity:layer'] === "string" ) ? [subject['aster-conformity:layer']]: subject['aster-conformity:layer'];
                formattedService['gx:aggregationOf'] = subject.hasOwnProperty('gx:aggregationOf') ? subject['gx:aggregationOf'] : "";
            };
            if(subject) {
                const type = subject['type'] || subject['@type'];
                switch (type) {
                    case 'aster-conformity:LocatedServiceOffering':
                        formattedService['gx:type'] = true;
                        formattedService['gx:logo'] = true;
                        break;
                    case 'aster-conformity:Location':
                        let allCountry = '';
                        if (type === 'aster-conformity:Location') {
                            const country = subject['aster-conformity:country'] ? subject['aster-conformity:country']: undefined;
                            const state = subject['aster-conformity:state'] ? subject['aster-conformity:state'] : undefined;
                            const urbanArea = subject['aster-conformity:urbanArea'] ? subject['aster-conformity:urbanArea'] : undefined;

                            if (urbanArea === state) {
                                allCountry = `${country}, ${state}`;
                            } else {
                                allCountry = `${country}, ${state}, ${urbanArea}`;
                            }
                        }
                        formattedService['gx:countries'] = allCountry;
                        break;
                    case 'aster-conformity:grantedLabel':
                        const label = subject['label']['hasName'] ? subject['label']['hasName']: undefined;
                         const match = (label.toString()).match( /Level (\d+)/);
                        formattedService['compliance_label_level'] = match ? match[1] : undefined;
                        break;
                    default:
                        break;
                }
                if (subject['type']) {
                    switch (subject['type']) {
                        case 'gx:ServiceOffering':
                            setTypeProperties('gx:ServiceOffering');
                            break;
                        case 'gx:LegalParticipant':
                            formattedService['gx-service-offering:providedBy'] = subject['gx:legalName'] ? subject['gx:legalName'] : undefined;
                            break;
                        default:
                            break;
                    }
                } else if (subject['@type']) {
                    switch (subject['@type']) {
                        case 'gx:ServiceOffering':
                            setTypeProperties('gx:ServiceOffering');
                            break;
                        case 'gx:LegalParticipant':
                            formattedService['gx-service-offering:providedBy'] = subject['gx:legalName'] ? subject['gx:legalName'] : undefined;
                            break;
                        default:
                            break;
                    }
                }
            }
        });
        formattedService['certif'] =certif;
        return formattedService;
    }

    const [service, setService] = useState(transformService(props));

    const handleNegocationClick = async () => {
        if (keycloak && keycloak.authenticated) {
            setLoading(true);
            setLoadingS(false);
            const result = await negotiationTool.initiateNegotiation(props.service);
            if (result.success) {
                this.props.history.push('http:localhost:8080')
            } else {
                setModal(true);
            }
            setLoading(false)
        } else if (keycloak && !keycloak.authenticated) {
            keycloak.login();
        }
    }

    const handleSubscribeClick = async () => {
        if (keycloak && keycloak.authenticated){
            setLoadingS(true);
            setLoading(false);
            const result = await negotiationTool.initiateSubcription(props.service);
            if (result.success) {
                setValueDialog("We would like to thank you for your support. An automated email containing the contract to be signed has been successfully sent.");
                setshowMessage(true)
            } else {
                setModal(true);
            }
            setLoadingS(false)
        } else if (keycloak && !keycloak.authenticated) {
            keycloak.login();
        }
    }

    function extractURLParts(input) {
        const match1 = input.match(/did:web:(.*):participant\/(.*)\/(.*)/i);
        const match2 = input.match(/did:web:(.*):participant:(.*)\/(.*)/i);
        if (match1) {
            return  `https://${match1[1]}/${`participant/${match1[2]}/${match1[3]}`}`;
        } else if (match2) {
            return `https://${match2[1]}/${`participant/${match2[2]}/${match2[3]}`}`;
        } else {
            return input;
        }
    }

    const handleSeeVC= async () => {

        const data = props.service;
        data.forEach((vc) => {
            const subject = vc.credentialSubject;
           if( subject &&
            ((subject["type"] || subject["@type"]) === "gx:ServiceOffering" )){
               const didUrl=vc['id'] || vc['@id'];
               window.open(extractURLParts(didUrl), "_blank");
           }
        })
    }


    const checkVC = async () => {
        try {
            setLoadVerifVc(true)
            const arrayWithoutComplianceCriteria = props.service.filter(objet => objet.type !== 'aster-conformity:ComplianceReference');
            const result = await api.checkVC(arrayWithoutComplianceCriteria);
            setResponseVerifierVC(result);
            setVerifyVcModal(true)
            setLoadVerifVc(false)
        } catch (error) {
            console.error(error.message);
            setLoadVerifVc(false)
        }
    };

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleCloseMessage = () => {
        setshowMessage(false);
    };

    useEffect(() => {
        setService(transformService(props));
    }, [props.service])

    return (
        <div className="row">
            { showMessage ? <Dialog
                    open={showMessage}
                    onClose={handleCloseMessage}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Message"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {
                                valuedialog
                            }
                        </DialogContentText>
                    </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseMessage} autoFocus>Close</Button>
                </DialogActions>
                </Dialog> : " "
            }

            { 
                <VerifyVcModal isOpen={verifyVcModal} closeModal={handleCloseDialog} responseVerifierVC={responseVerifierVC} />
            }

            <div className='col-12'>
                <div className="card">
                    <div className='card-body w-100'>
                        <div className="me-4 d-flex align-items-center mt-0 d-md-none d-block">
                                {!service["gx:logo"]?
                                        <img className='img-service' alt="Gaia-x logo" src={aster_ces}></img>
                                :  <img className='img-service' alt="Gaia-x logo" src={aster}></img>}
                            </div>
                            <div className="d-flex justify-content-center d-md-none d-block mt-3">
                            <div>
                                <div className='d-flex flex-column'>
                                    {service["gx:complianceType"] ?
                                            <div className='position-relative me-0 me-sm-2 mb-2 mb-sm-0 text-center'>
                                                <Tooltip title="Gaia-X Compliance" >
                                                    <span>
                                                        {service["gx:complianceType"] === "v1" ?
                                                            <div>
                                                                <span className='position-absolute label-title'>Gaia-X Compliance</span>
                                                                <p className='green-label px-2 rounded-2'>Prod {service["gx-service-offering:complianceVersion"]}</p>
                                                            </div>
                                                        : (service["gx:complianceType"] === 'Development' || service["gx:complianceType"] === 'V1-staging') ?
                                                            <div>
                                                                <span className='position-absolute label-title'>Gaia-X Compliance</span>
                                                                <p className='orange-label px-2 rounded-2'>Dev {service["gx-service-offering:complianceVersion"]}</p>
                                                            </div>
                                                        : null
                                                        }
                                                    </span>
                                                </Tooltip>
                                            </div>
                                    : ""}

                                    {service.compliance_label_level ?
                                        <div className='position-relative me-0 me-sm-2 text-center'>
                                            <Tooltip title="Label">
                                                <span>
                                                    <span className='position-absolute label-title'>Label</span>
                                                    <p className='label px-2 rounded-2'>{service.compliance_label_level === 0 ? "" : "Aster- " + service.compliance_label_level}&nbsp;{service.compliance_label_level === 1 ? <><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg></> : "" || service.compliance_label_level === 2 ? <><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg></> : "" || service.compliance_label_level === 3 ? <><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg></> : ""}</p>
                                                </span>
                                            </Tooltip>
                                        </div>
                                    : ""}
                                </div>

                                <div className="position-relative">
                                    <div className="pb-1">
                                        <p className='s-name service-name me-0 me-sm-4 h-100' >{service["gx-service-offering:title"]}</p>
                                        {!service['gx:type']?
                                            <span className='ces-label position-absolute'>{"From CES"}</span>
                                            : <span className='ces-label position-absolute'>{" "} </span>}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 col-md-1 px-md-0 text-center d-none d-md-block">
                                {!service["gx:logo"]?
                                    <img className='img-service' alt="Gaia-x logo" src={aster_ces}></img>
                                :  <img className='img-service' alt="Gaia-x logo" src={aster}></img>}
                            </div>
                            <div className="col-12 col-sm-8 col-md-12 col-lg-9 pt-2 ps-sm-5 ps-md-4 ps-lg-0">
                                <div className="row d-none d-md-block">
                                    <div className="col-12 d-flex   justify-content-start">
                                        <div className="position-relative">
                                            <p className='s-name service-name me-4'>{service["gx-service-offering:title"]}</p>
                                            {!service['gx:type']?
                                                <span className='ces-label position-absolute'>{"From CES"}</span>
                                                : <span className='ces-label'>{" "} </span>}
                                        </div>

                                        <div className='d-flex justify-content-start'>
                                            {service["gx:complianceType"] ?
                                                    <div className='position-relative me-2'>
                                                        <Tooltip title="Gaia-X Compliance">
                                                            <span>
                                                                {service["gx:complianceType"] === "v1" ?
                                                                    <div>
                                                                        <span className='label-title position-absolute'>Gaia-X Compliance</span>
                                                                        <p className='green-label px-5 rounded-2'>Prod {service["gx-service-offering:complianceVersion"]}</p>
                                                                    </div>
                                                                : (service["gx:complianceType"] === 'Development' || service["gx:complianceType"] === 'V1-staging') ?
                                                                <div>
                                                                    <span className='label-title position-absolute'>Gaia-X Compliance</span>
                                                                    <p className='orange-label px-5 rounded-2'>Dev {service["gx-service-offering:complianceVersion"]}</p>
                                                                </div>
                                                                : null
                                                                }
                                                            </span>
                                                        </Tooltip>
                                                    </div>
                                            : ""}

                                            {service.compliance_label_level ?
                                                <div className='position-relative me-2'>
                                                    <Tooltip title="Label">
                                                        <span>
                                                            <span className='label-title position-absolute'>Label</span>
                                                            <p className='label px-4 rounded-2'>{service.compliance_label_level === "0" ? "" : "Aster- " + service.compliance_label_level}&nbsp;{service.compliance_label_level === "1" ? <><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg></> : "" || service.compliance_label_level === "2" ? <><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg></> : "" || service.compliance_label_level === "3" ? <><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg></> : ""}</p>
                                                        </span>
                                                    </Tooltip>
                                                </div>
                                            : ""}
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-12">
                                        <div className='d-md-inline-flex d-md-flex justify-content-start mt-1 mt-sm-4 mt-md-1 w-100'>

                                            <div className='d-flex justify-content-start'>
                                                <img className='icon' alt="provider icon" width="16px" height="16px" src={providerIcon}></img>&nbsp;<p className='me-4 fw-semibold'>{service['gx-service-offering:providedBy'] ? service['gx-service-offering:providedBy'] : "Unknown"}</p>
                                            </div>
                                            {
                                                (service['gx-service-offering:serviceType'] !== undefined) ?
                                                    (service['gx-service-offering:serviceType'].length !== 0) ?
                                                <div className='d-flex justify-content-start'>
                                                    <img className='icon' alt="cube icon" width="16px" height="16px" src={settings}></img>&nbsp;<p className='type-service me-4 fw-semibold'>{service['gx-service-offering:serviceType'].toString().replaceAll(',', ', ')}</p>
                                                </div>
                                                : "":""
                                            }
                                            { service['gx-service-offering:layer'] && service['gx-service-offering:layer'].length !== 0 ?
                                                <div className='d-flex justify-content-start'>
                                                    <img className='icon' alt="cube icon" width="16px" height="16px" src={cube}></img>&nbsp;<p className='fw-semibold me-4'>{service["gx-service-offering:layer"] ? " " + service["gx-service-offering:layer"] : ""}</p>
                                                </div>
                                            :
                                                ""
                                            }

                                            {service['gx:countries'] ? (
                                                <div className='d-flex justify-content-start'>
                                                    <img className='icon' alt="cube icon" width="16px" height="16px" src={globe} />&nbsp;<p className='fw-semibold'>{service['gx:countries']}</p>
                                                </div>
                                            ) : null}
                                        </div>

                                    </div>

                                    <div className="col-12">
                                        {service['gx-service-offering:description'] && (
                                            <div className='mb-4' >
                                                <div >
                                                    {service['gx-service-offering:description'].length > 150 ? (
                                                        <div className="row ">
                                                            <div className="me-1 col-auto p-0" style={{marginTop:"-1px"}}> <Button variant="contained" className="mt-1" style={{color:"black",marginTop:"-2px",padding:"0px",backgroundColor:'white', fontWeight: 'bold'}} onClick={() => setShowMore(!showMore)}>
                                                                    {showMore ?
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-chevron-up " viewBox="0 0 16 16">
                                                                            <path fillRule="evenodd" d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z"/>
                                                                        </svg>
                                                                        :
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-chevron-down" viewBox="0 0 16 16">
                                                                            <path fillRule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                                                        </svg>
                                                                    }
                                                                </Button></div>
                                                            <div className="col p-0">
                                                                {showMore
                                                                    ? service['gx-service-offering:description']
                                                                    : service['gx-service-offering:description'].substring(0, 150)+"..."}
                                                                {!showMore && (
                                                                    <span style={{ display: 'none' }}>
                      {service['gx-service-offering:description'].substring(150)}
                    </span>
                                                                )}
                                                            </div>

                                                        </div>
                                                    ) : (
                                                        <p>{service['gx-service-offering:description']}</p>
                                                    )}
                                                </div>
                                            </div>
                                        )}
                                        <div className='d-sm-block d-md-flex'>
                                            {service['certif'].map((refL, index) => <div key={index} className='mt-2 norme d-flex me-4'>
                                                <p className='me-1'><svg className={refL ? 'round green' : 'none'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></p>
                                                <p className='d-flex flex-column align-items-center'>{refL}</p>
                                            </div>)}
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col-sm-12 col-lg-2 d-flex align-items-center">
                                <Button onClick={handleShow} className="me-2 btn-see-more w-100">
                                    See more
                                </Button>
                            </div>
                        </div>


                    <Offcanvas className="view-service" show={show} onHide={handleClose} {...props} placement={"end"}>

                        <Offcanvas.Header className='closeCross d-flex align-items-start justify-content-start' closeButton style={{marginLeft:'-7px'}}></Offcanvas.Header>
                        <div className='d-flex justify-content-start'>
                            <div className='imgService service-border p-2 d-flex align-items-center'>
                                {!service["gx:logo"]?
                                    <img className='img-service' alt="Gaia-x logo" src={aster_ces}></img>
                                    :  <img className='img-service' alt="Gaia-x logo" src={aster}></img>}
                            </div>
                            <div className='serviceName d-flex flex-column justify-content-center'>
                                <div className='d-flex justify-content-start mb-0 mt-0'>
                                    {service["gx:complianceType"] ?
                                        <Tooltip title="Gaia-X Compliance">
                                            <div className='position-relative mb-2 me-3'>
                                                {
                                                service["gx:complianceType"] === "v1" ? 
                                                    <div>
                                                        <span className='position-absolute label-title-section mb-0'>Gaia-X Compliance</span>
                                                        <p className='green-label px-2 rounded-2 mb-0 mt-0'>Prod {service["gx-service-offering:complianceVersion"]}</p>
                                                    </div>
                                                : (service["gx:complianceType"] === 'Development' || service["gx:complianceType"] === 'V1-staging') ?
                                                    <div>
                                                        <span className='position-absolute label-title-section mb-0'>Gaia-X Compliance</span>
                                                        <p className='orange-label px-2 rounded-2 mb-0 mt-0'>Dev {service["gx-service-offering:complianceVersion"]}</p>
                                                    </div>
                                                : null
                                                }
                                            </div>
                                        </Tooltip>
                                    : ""}

                                    {service.compliance_label_level ?
                                        <Tooltip title="Label">
                                             <div className='position-relative mb-2'>
                                                <span className='position-absolute label-title-section'>Label</span>
                                                <p className='label px-2 rounded-2 text-center mb-0'>{service.compliance_label_level === "0" ? "" : "Aster- " + service.compliance_label_level}&nbsp;{service.compliance_label_level === "1" ? <><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg></> : "" || service.compliance_label_level === "2" ? <><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg></> : "" || service.compliance_label_level === "3" ? <><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className="stars" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg></> : ""}</p>
                                            </div>
                                        </Tooltip>
                                    : ""}                               
                                </div>

                                <div className='mb-0'>
                                    <p className='service-title mb-0'>{service['gx-service-offering:title']}</p>
                                    {!service['gx:type']?
                                        <span className='ces-label ces-label-margin'>{"From CES"}</span>
                                        : <span className='ces-label ces-label-margin'>{" "} </span>}
                                </div>
                            </div>
                        </div>

                        <div className='desc-service'>
                            {service['gx-service-offering:description'] && (
                                <div className='mb-4' >
                                    <div className='d-flex justify-content-start align-items-center'>
                                        <p className='me-2 d-flex align-items-center'>
                                            <img alt="cube icon" width="16px" height="16px" src={cube}></img>
                                        </p>
                                        <p className='federation' style={{color:'white'}}>Description</p>
                                    </div>
                                    <div className="me-1">
                                        {service['gx-service-offering:description'].length > 150 ? (
                                            <div className="row"> <div className="ms-1 col-auto " >   <Button   style={{color:"white",marginTop:"-1px",padding:"0px",backgroundColor:'#000F8E', fontWeight: 'bold'}} onClick={() => setShowMore(!showMore)}>
                                                {showMore ?
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-chevron-up " viewBox="0 0 16 16">
                                                        <path fillRule="evenodd" d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z"/>
                                                    </svg>
                                                    :
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-chevron-down" viewBox="0 0 16 16">
                                                        <path fillRule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                                    </svg>
                                                }
                                            </Button></div>
                                                <div className='t-white col p-0 me-4 ms-0'>
                                                    {showMore
                                                        ? service['gx-service-offering:description']
                                                        : service['gx-service-offering:description'].substring(0, 150)+"..."}
                                                    {!showMore && (
                                                        <span style={{ display: 'none' }}>
                      {service['gx-service-offering:description'].substring(150)}
                    </span>
                                                    )}
                                                </div>

                                            </div>
                                        ) : (
                                            <p className='t-white'>{service['gx-service-offering:description']}</p>
                                        )}
                                    </div>
                                </div>
                            )}
                            <p className='t-white download' onClick={handleSeeVC}>
                                <svg width="22" height="16" viewBox="0 0 24 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fillRule="evenodd" clipRule="evenodd" d="M2.3644 9.84311C2.14027 9.49606 1.9691 9.20588 1.85273 9C1.9691 8.79411 2.14027 8.50394 2.3644 8.15689C2.83126 7.43402 3.523 6.47205 4.42215 5.51296C6.23414 3.58017 8.79963 1.75 12 1.75C15.2004 1.75 17.7659 3.58017 19.5778 5.51296C20.477 6.47205 21.1687 7.43402 21.6356 8.15689C21.8597 8.50394 22.0309 8.79411 22.1473 9C22.0309 9.20588 21.8597 9.49606 21.6356 9.84311C21.1687 10.566 20.477 11.5279 19.5778 12.487C17.7659 14.4198 15.2004 16.25 12 16.25C8.79963 16.25 6.23414 14.4198 4.42215 12.487C3.523 11.5279 2.83126 10.566 2.3644 9.84311ZM23.6705 8.66404C23.6707 8.66433 23.6708 8.66459 23 9C23.6708 9.33541 23.6707 9.33567 23.6705 9.33596L23.6702 9.3367L23.6692 9.33871L23.666 9.34492L23.6553 9.36587C23.6462 9.38357 23.6332 9.40865 23.6163 9.44063C23.5824 9.50459 23.533 9.59617 23.4681 9.71145C23.3385 9.94189 23.1471 10.2676 22.8957 10.6569C22.3938 11.434 21.648 12.4721 20.6722 13.513C18.7341 15.5802 15.7996 17.75 12 17.75C8.20037 17.75 5.26586 15.5802 3.32785 13.513C2.352 12.4721 1.60624 11.434 1.10435 10.6569C0.852934 10.2676 0.661492 9.94189 0.531865 9.71145C0.467023 9.59617 0.417564 9.50459 0.383723 9.44063C0.3668 9.40865 0.353774 9.38357 0.344675 9.36587L0.333974 9.34492L0.330837 9.33871L0.329823 9.3367L0.329455 9.33596C0.32931 9.33567 0.32918 9.33541 1 9C0.32918 8.66459 0.32931 8.66433 0.329455 8.66404L0.329823 8.66331L0.330837 8.66129L0.333974 8.65508L0.344675 8.63413C0.353774 8.61643 0.3668 8.59135 0.383723 8.55937C0.417564 8.49541 0.467023 8.40383 0.531865 8.28855C0.661492 8.05811 0.852934 7.73239 1.10435 7.34311C1.60624 6.56598 2.352 5.52795 3.32785 4.48704C5.26586 2.41983 8.20037 0.25 12 0.25C15.7996 0.25 18.7341 2.41983 20.6722 4.48704C21.648 5.52795 22.3938 6.56598 22.8957 7.34311C23.1471 7.73239 23.3385 8.05811 23.4681 8.28855C23.533 8.40383 23.5824 8.49541 23.6163 8.55937C23.6332 8.59135 23.6462 8.61643 23.6553 8.63413L23.666 8.65508L23.6692 8.66129L23.6702 8.66331L23.6705 8.66404ZM1 9L0.32918 9.33541C0.223607 9.12426 0.223607 8.87574 0.32918 8.66459L1 9ZM23 9L23.6708 8.66459C23.7764 8.87574 23.7764 9.12426 23.6708 9.33541L23 9ZM9.75 9C9.75 7.75736 10.7574 6.75 12 6.75C13.2426 6.75 14.25 7.75736 14.25 9C14.25 10.2426 13.2426 11.25 12 11.25C10.7574 11.25 9.75 10.2426 9.75 9ZM12 5.25C9.92893 5.25 8.25 6.92893 8.25 9C8.25 11.0711 9.92893 12.75 12 12.75C14.0711 12.75 15.75 11.0711 15.75 9C15.75 6.92893 14.0711 5.25 12 5.25Z" fill="#FFFFFF"/>
                                </svg>
                                &nbsp;<span className='underline'>See the VC</span>
                            </p>
                        </div>
                        <Offcanvas.Body className='body-show-canvas'>

                            <div className=" row m-2" >
                                <Button style={{backgroundColor:"#000F8E"}}  onClick={checkVC} >Verify VC</Button>
                            </div>
                            <div>
                            <Backdrop
                                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                                open={loadVerifVc}
                                onClick={handleCloseDialog}
                            >
                                <CircularProgress color="inherit" />
                            </Backdrop></div>
                            <div className='mb-4'>
                                <div className='d-flex justify-content-start align-items-center'>
                                    <p className='me-2 d-flex align-items-center'><img alt="provider icon" width="16px" height="16px" src={providerIcon}></img></p>
                                    <p className='federation'>Provider</p>
                                </div>
                                <div>
                                    <Button className='btnService'>{service['gx-service-offering:providedBy'] ? service['gx-service-offering:providedBy'] : "Unknown"}</Button>
                                </div>
                            </div>

                            <div className='mb-4'>
                                <p className='federation'><img alt="settings icon" width="16px" height="16px" src={settings}></img> Type of service</p>
                                <div>
                                    {service['gx-service-offering:serviceType'] !== undefined ? service['gx-service-offering:serviceType'].map((refL, index) => <Button key={index} className='btnService'>{refL ? refL : "Unknown"}</Button>):""}
                                </div>
                            </div>


                            {service['gx-service-offering:serviceType'] !== undefined  && service['gx-service-offering:serviceType'].includes("Data Product")?
                                <div className='section-filter'>
                                    <p className='federation'><svg className='gear white mr' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M448 80v48c0 44.2-100.3 80-224 80S0 172.2 0 128V80C0 35.8 100.3 0 224 0S448 35.8 448 80zM393.2 214.7c20.8-7.4 39.9-16.9 54.8-28.6V288c0 44.2-100.3 80-224 80S0 332.2 0 288V186.1c14.9 11.8 34 21.2 54.8 28.6C99.7 230.7 159.5 240 224 240s124.3-9.3 169.2-25.3zM0 346.1c14.9 11.8 34 21.2 54.8 28.6C99.7 390.7 159.5 400 224 400s124.3-9.3 169.2-25.3c20.8-7.4 39.9-16.9 54.8-28.6V432c0 44.2-100.3 80-224 80S0 476.2 0 432V346.1z" /></svg>Data product</p>
                                    {service["gx:aggregationOf"]?.map((aggregation, index) =>
                                        <DataSet key={index} service={aggregation} />
                                    )}
                                </div>
                                : ""
                            }
                            {service['gx-service-offering:layer'] ?
                                <div className='mb-4'>
                                    <div className='d-flex justify-content-start align-items-center'>
                                        <p className='me-2 d-flex align-items-center'><img alt="cube icon" width="16px" height="16px" src={cube}></img></p>
                                        <p className='federation'>Layer</p>
                                                                 </div>
                                    {service['gx-service-offering:layer'].length > 0 ?
                                        <div>
                                            {service['gx-service-offering:layer'].map((option, index) => {
                                                return <Button key={index} className='btnService'>{option}</Button>;
                                            })}
                                        </div>
                                        : ""}
                                </div>
                                : ""}

                            {service["gx:countries"]?.length > 0 ?
                                <div className='mb-4'>
                                    <div className='d-flex justify-content-start align-items-center'>
                                        <p className='me-2 d-flex align-items-center'><img alt="earth icon" width="16px" height="16px" src={globe}></img></p>
                                        <p className='federation'>location</p>
                                    </div>
                                    <div>
                                        <Button className='btnService'>{service["gx:countries"]?.length > 0 ? service["gx:countries"] : ""}</Button>
                                    </div>
                                </div>
                            : ""}
                            <div className='section-filter'>
                                <div className='d-flex justify-content-start align-items-center'>
                                    <p className='me-2 d-flex align-items-center'><img alt="home icon" width="16px" height="16px" src={check}></img></p>
                                    <p className='federation'>Compliance references and other criteria claims</p>
                                </div>
                                <div>
                                    {service['certif'].map((refL, index) => <Button key={index} className='btnService'><svg className={refL ? 'round small green' : 'round small red'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg> {refL ? refL : "Unknown"}</Button>)}
                                </div>
                            </div>

                            <div className="button-container">

                                {service['gx-service-offering:providedBy'] === 'AgDataHub' ?   <Button className="btn-see-more" onClick={handleSubscribeClick} disabled={loadingS}>
                                {loadingS ? <img className="loading-gif" src={loadingGif} alt="Loading..." />
                                    : "Subscribe"}
                                </Button>:

                                    <Button className="btn-see-more" onClick={handleSubscribeClick} disabled={true}>
                                {loadingS ? <img className="loading-gif" src={loadingGif} alt="Loading..." />
                                    : "Subscribe"}
                                </Button>}

                                {service['gx-service-offering:providedBy'] === 'AgDataHub' && (
                                    <Button className="btn-see-more" onClick={handleNegocationClick} disabled={loading}>
                                        {loading ? <img className="loading-gif" src={loadingGif} alt="Loading..." /> : "Negociate contract"}
                                    </Button>
                                )}
                            </div>

                            <Modal show={showModal} onHide={() => setShowModal(false)}>
                                <Modal.Body>
                                <div style={{ textAlign: 'center' }}>ok</div>
                                </Modal.Body>
                            </Modal>

                        </Offcanvas.Body>
                    </Offcanvas>
                </div>
            </div>
        </div>
    </div>
    )
}
