# Description of the Web Catalogue



# Table of Contents
### 1: [Introduction](#introduction)

 1.1 [Presentation of the service web catalog](#catalogue-presentation)

 1.2 [Context](#context)

 1.3 [Objectives of the web catalog](#objectives)

 1.4 [Architecture of the web Catalog](#architecture)

 1.5 [Functioning of the web catalog](#Functioning-catalog)


### 2: [Overview of the web Catalog](#overview-webCatalog)

 2.1 [Presentation of services](#presentation-services)

 2.2 [Presentation of a verifiable credential for a service](#presentation-vc)

 2.3 [Filtering options](#filtering)

 2.4 [Advanced search features](#advanced-search)

 2.5 [Description of services](#description-services)

 2.6 [Connecting and Consuming services in the web catalog](#connecting-catalogue)

### 3: [Technical Specification](#technical-pecification)

 3.1 [Technologies and tools used](#technologies)

 3.2 [Step to install the project locally](#step-install)

### 4: [Authors and acknowledgment](#authors-and-acknowledgment)

### 5: [License](#licence)

## 1. Introduction <a name="introduction"></a>

### 1.1. Presentation of the service web catalog<a name="catalogue-presentation"></a>

The online service catalogue we are presenting is a platform that brings together a diverse range of services
from a number of providers, all in a secure environment. The aim of the catalogue is to simplify consumers'
lives by offering them an easy way to explore, compare, subscribe to and negotiate a variety of services,
all in one place. The online catalogue offers a wide range of services of different types (data product, storage, VPN, VPS, etc.),
some of which have one or more certifications. The services available in the catalogue are subject to Gaia-X compliance requirements.

### 1.2. Context<a name="contexte"></a>

In the context of the Gaia-X project, the aim is to make available an open source reusable catalogue module
for dataspaces. This will enable participating suppliers to publish their services in a secure environment.
The aim of this platform was to facilitate the circulation and consumption of data,At the same time, we ensure
that company services respect the compliance standards 
set by Gaia-X and the dataspaces we use as references.

Our web catalogue of online services was created to meet this challenge by offering a decentralised
solution. In this model, our federated catalogue is fed with the various services from 
suppliers' catalogues, which are hosted on their platforms.
The information is then extracted by the federated catalogue to feed Aster-X catalogue.

For the sake of documentation, the online web catalog we present corresponds to that of the Aster-X 
federation, and will serve as an illustrative example in the following sections.

Our catalogue is intended to be made available to 
the dataspace, which can modify it or add information according to its needs.

The creation of the web catalogue considerably simplifies the process of searching for, 
subscribing to and even negotiating available data services from a number of suppliers.

All the services retrieved from suppliers' catalogues and included in our online
catalogue are accompanied by descriptions that comply with Gaia-X standards.

The ultimate aim is to promote data sovereignty and interoperability.

To access the online catalogue, please click on the following link: https://webcatalog.aster-x.demo23.gxfs.fr/catalog

Below is an picture of the catalogue home page, highlighting a variety of available services:

![Texte alternatif](./image/accueil.png)

### 1.3. Objectives of the web catalog <a name="objectives"></a>

1) Provide an overview of services from several providers.

2) Simplify the search for specific services and data.

3) Provide information on service labels and compliance.

4) Enable comparison of label levels, location and service descriptions against a service type.

5) Facilitate consumer access to data and services.

6) Allow subscription and negotiation of data and services.


### 1.4. Architecture of the web Catalog<a name="architecture"></a>

![Texte alternatif](./image/schema_catalogue.png)

For further details on the service consumption process, please click on the following link: 
 https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/main/scenarii.md.

### 1.5 Functioning of the web catalog<a name="Functioning-catalog"></a>

Service information and catalog data are integrated using an available tool called the Wizard Tool.

Wizard Tool is the entry point for service providers to describe and create their services. 
The Wizard Tool can be seen as a tool to help service providers creating their VPs and VCs which are 
mandatory to publish them to the Federated Catalogue with compliance requirements and Aster-X label  based on Gaia-X labelling rules.

For more information on the Wizard, please refer to the following link:
https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/service-description-wizard-tool.

This is the home page of the wizard, showing the services available and published in the web catalog.
You can add services by clicking on the "Add service description" button:

![Texte alternatif](./image/wizard.png)

Below is the interface for describing a service in the Wizard tool:

![Texte alternatif](./image/wizard2.png)


## 2. Overview of the web Catalog<a name="overview-webCatalog"></a>

### 2.1. Presentation of services <a name="presentation-services"></a>

The services listed in the web catalogue comprise a number of elements, described below.

We'll use the example of the Digital Signature service below to illustrate these components:

 ![Texte alternatif](./image/services.png)

1)  The name **" CONTRALIA-API "** : Corresponds to the service name

2)  **" Dev 22.10 "** : the green color indicates Gaia-X compliance in production, while orange color indicates Gaia-X compliance in development.

3) **" Aster-2 "** : represents the label level assigned to the service.

4)  **" DOCAPOSTE "** : is the name of the provider offering this service.

5)  **" Digital Signature "** : describes the type of service.

6)  **" IAAS "** : indicates the service delivery model, which is a software-as-a-service model.

7) **" CONTRALIA-API is a service offered by DOCAPOSTE "** : is the description of the service, indicating that it is a backup service offered by ARUBA.

9)  **" SWIPO SaaS Code , ISO 27701 , ISO 20000, ISO 50001 "** : refers to the various certifications held by the service.

10)  The **" See more "** : button gives access to additional information about the service.


### 2.2 Presentation of a verifiable credential for a service<a name="presentation-vc"></a>
Services listed in the online web catalog are extracted from the federated catalog in the form of verifiable service credentials.
The aim is to make the various services transparent and verifiable by the consumer. 
Consumers can download this verifiable credential directly from the catalog. Here's an example of such a verifiable credential: :

```
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    {
      "gx-data": "https://www.w3.org/2018/credentials/v1"
    }
  ],
  "@id": "did:web:montblanc-iot.provider.gaia-x.community:participant:39100b2d1113ff9136dc29a65dcae51f48e73735df72e38c1c375f3c7a08ba87/vc/4cf886deee1fbf25bf0d4fc5584ae1f9a3f061d546d0a0c5d4701bcb736948be/data.json",
  "type": [
    "VerifiableCredential",
    "ServiceOfferingExperimental"
  ],
  "issuer": "did:web:montblanc-iot.provider.gaia-x.community",
  "expirationDate": "2023-08-01T15:03:23.688+02:00",
  "credentialSubject": {
    "id": "did:web:montblanc-iot.provider.gaia-x.community:participant:39100b2d1113ff9136dc29a65dcae51f48e73735df72e38c1c375f3c7a08ba87/vc/4cf886deee1fbf25bf0d4fc5584ae1f9a3f061d546d0a0c5d4701bcb736948be/data.json",
    "gx-service-offering:providedBy": "https://montblanc-iot.provider.gaia-x.community/participant/39100b2d1113ff9136dc29a65dcae51f48e73735df72e38c1c375f3c7a08ba87/vc/legalperson/data.json",
    "gx-terms-and-conditions:serviceTermsAndConditions": {
      "gx-terms-and-conditions:value": "https://compliance.lab.gaia-x.eu/development",
      "gx-terms-and-conditions:hash": "myrandomhash"
    },
    "gx-service-offering:title": "MontBlancDP",
    "gx-service-offering:serviceType": [
      "Data Product"
    ],
    "gx-service-offering:description": "Data Product",
    "gx-service-offering:descriptionMarkDown": "",
    "gx-service-offering:webAddress": "",
    "gx-service-offering:dataProtectionRegime": "",
    "gx-service-offering:dataExport": [
      {
        "gx-service-offering:requestType": "email",
        "gx-service-offering:accessType": "digital",
        "gx-service-offering:formatType": "mime/png"
      }
    ],
    "gx-service-offering:dependsOn": "",
    "gx-service-offering:keyword": "",
    "gx-service-offering:layer": "PAAS",
    "gx-service-offering:isAvailableOn": [
      ""
    ],
    "@type": "gx-data:DataProduct",
    "gx-data:dataDomains": [
      "Domain1"
    ],
    "gx-data:dataController": "",
    "gx-data:consent": "",
    "gx-data:policies": "",
    "gx-data:obsoleteDateTime": null,
    "gx-data:expirationDateTime": null,
    "gx-data:aggregationOf": [
      {
        "gx-data:identifier": "532eaabd9574880dbf76b9b8cc00832c20a6ec113d682299550d7a6e0f345e25",
        "gx-data:title": "Test",
        "gx-data:description": "",
        "gx-data:copyrightOwnedBy": [
          "Owner"
        ],
        "gx-data:personalDataPolicy": "",
        "gx-data:expirationDateTime": null,
        "gx-data:obsoleteDateTime": null,
        "gx-data:distribution": [
          {
            "gx-data:title": "test",
            "gx-data:mediaType": "CSV",
            "gx-data:byteSize": "2500",
            "gx-data:location": "/etc/bin",
            "gx-data:fileHash": "",
            "gx-data:algorithm": "PS256",
            "gx-data:expirationDateTime": null,
            "gx-data:obsoleteDateTime": null
          }
        ]
      }
    ]
  },
  "issuanceDate": "2023-05-03T13:04:18.052657+00:00",
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:montblanc-iot.provider.gaia-x.community",
    "created": "2023-05-03T13:04:18.052657+00:00",
    "jws": "eyJhbGciOiAiUFMyNTYiLCAiYjY0IjogZmFsc2UsICJjcml0IjogWyJiNjQiXX0..RcPy_CYXi3cqxhwptBikH_ztpWMlBDOORDihNH-oxCzSCVwDmacjPxJE4ybQBylL1TlZc_e_99WSuOQbuGHFpwdzwKgxaogM80HJev9HyL-o0h2-krEcWIcxmE5Z1LNYJlLX9Ukf0TuNPOehvlJHfIAzDXOTvAz_KMJMm1_Y13aTOAVXv8DH25eZ-bA-Gmemo_MZ7DVvrtYm6dvM_BAUVvlMXKo9kx0fBSAdwwODLrmvGne18SWIMI4MzEqJJ09uuQDOtTGXUo0kOVYtUVVqm03EVxrqvJRRQCjmQE8B_gJmwoCe4X6Ddwmu91TOSvQw6zy9aYow_zfz8HsNcizeLA"
  },
  "layer": "['PAAS']",
  "service_type": "['Data Product']",
  "service_title": "MontBlancDP"
}
```


### 2.3. Filtering options<a name="filtering"></a>

The filter button in the web catalogue allows users to restrict or customise their search 
results according to specific criteria. It offers users the opportunity to sort and present 
information in a more targeted way, which can make searching more efficient and relevant.

Below are the different elements contained in the web catalogue filter:

![Texte alternatif](./image/filtre.png)

### 2.4. Advanced search features<a name="advanced-search"></a>

The web catalogue search function allows users to carry out precise searches to find the services that interest them. An example is shown in the picture below:

![Texte alternatif](./image/search.png)

### 2.5. Description of services<a name="description-services"></a>

The services listed in the online web catalogue contain a wealth of detailed
information that you can explore by clicking on the "See more" button for the 
corresponding service.
In our online web catalog, we list different types of services, and
the services are accompanied by detailed descriptions that vary according to their type. 
For example, dataproduct services include datasets and data distributions, while other 
service types may not.

Here is an picture of the description of the "Sophia 2023 DataProduct"
service of type "DATA PRODUCT":

![Texte alternatif](./image/Catalogue_description2.png)

Here is an picture of the description of the "CONTRALIA-API"
service of type "Digital Signature":

![Texte alternatif](./image/Catalogue_description.png)


### 2.6. Connecting and Consuming services in the web catalog<a name="connecting-catalogue"></a>

Here's how consumers connect to and use the services in the online catalogue:

```mermaid
sequenceDiagram
    actor Consumer
    participant Web Catalogue
    participant Keycloak
    participant Wallet
    participant Api Negotiation Tool

    Consumer->>Web Catalogue: Starting the connection session
    Web Catalogue->>Keycloak: Establishing a connection using Keycloak with your wallet
    Keycloak->>Wallet: Verifiable Presentation Request (VPR)
    Wallet->>Wallet: Connection to your wallet
    Wallet->>Wallet: Presentation of the EmployeeCredential and LegalParticipant VC
    Wallet->>Keycloak: Retrieval and verification of verifiable credentials (VC)
    Keycloak->>Consumer: provide access (token).
    Consumer->>Web Catalogue: Authorise consumer access to the Web Catalogue.
    Consumer->>Web Catalogue: data product search, subscription and negotiation using an authenticated session
    Web Catalogue->>Api Negotiation Tool: Start of negotiation or subscription process

```
Once a consumer is connected to the web catalogue, they can subscribe to a service or start negotiations by clicking on one of the buttons on the service description page, as shown in the picture illustrating the different actions available:

![Texte alternatif](./image/catalogue.png)

Actions highlighted in red trigger the negotiation workflow. You can consult the documentation by clicking on the link below:
https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/negociation-tool

## 3. Technical Specification<a name="technical-pecification"></a>

### 3.1. Technologies and tools used<a name="technologies"></a>

#### Programming languages used

1.  NodeJs
2.  Reactjs

#### The tools used

1. Wallet
2. Keycloak


The services listed in the catalogue are retrieved via this endpoint :

| HTTP Verb | url                                                                    | input | output | Description |
|---|------------------------------------------------------------------------|-------|---|---|
| HTTP POST | https://federated-catalogue-api.abc-federation.gaia-x.community/query  | n/a   | An array containing tables of verifiable credentials | retrieves the list of services |


Below is the link to the Swagger url providing all the possible queries for accessing services in the federated catalogue:
https://federated-catalogue-api.abc-federation.dev.gaiax.ovh/api/doc#

### 3.2. Step to install the project locally<a name="step-to-install-the-project-locally"></a>

####  Step 1

- Cloning the GitLab directory:

Start by opening your terminal (or command prompt) and
navigate to the directory where you want to clone the project.

Use the following command to clone the GitLab directory:

```
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2
```
####  Step 2
Installing the dependencies :

Once the project has been cloned, navigate to the project directory and install the necessary dependencies using the command :

```  
  npm install
```
####  Step 3

Starting the React.js project:

Once all the dependencies are installed, you can start the project locally using the command :
```
npm start
```
This command starts a development server and opens a new browser
window with your project running.

####  Step 4

Once the development server is running, you can access the
catalogue in your browser at http://localhost:3000.

```
http://localhost:3000
```
## 4. Authors and acknowledgment <a name="authors-and-acknowledgment"></a>
GXFS-FR

## 5. License <a name="licence"></a>
The web catalogue is delivered under the terms of the Apache License Version 2.0.





