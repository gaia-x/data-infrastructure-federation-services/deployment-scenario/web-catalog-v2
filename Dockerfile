FROM node:alpine

RUN addgroup -S appgroup
RUN adduser -S appuser -G appgroup

WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH


COPY package*.json ./
COPY *.js ./
COPY ./src ./src
COPY ./public ./public

RUN npm install --silent --ignore-scripts

USER appuser

EXPOSE 3000
CMD ["npm", "start"]